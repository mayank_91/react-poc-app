import React, { Component } from 'react'
import ImagesThumbnail from './ImagesThumbnail'
import axios from 'axios'

export default class ProductView extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             selectedSize: '64 GB',
             selectedColor: 'orange',
             details: []
        }
    }
    
    handleClickForSize = (event) =>{
        event.preventDefault()
    }

    // componentDidMount(){
    //     axios.get(`http://localhost:8080/info/${this.state.selectedSize}/${this.state.selectedColor}`)
    //         .then(response=>{
    //            this.setState({
    //             details: response.data
    //            })
    //         })
    //         .catch(error=>{
    //             console.log("error in ProductView.js")
    //         })
    // }

    render() {
        const { productConfiguration } = this.props;
        // const splittedDescription = description.split("|");
        // const desc = splittedDescription.map((desc, index)=><li key={index}>{desc}</li>)
        debugger;
        const size = productConfiguration.sizes.map((size, index)=><button className="size a-button a-button-selected a-button-thumbnail a-button-toggle" 
                                                                data-toggle="tooltip" 
                                                                title="small"
                                                                key={index}
                                                                onClick={this.handleClickForSize}
                                                                >{size.size}
                                                            </button>)
        // const color = productConfiguration.colors.map((color, index)=><button className={`color ${color.color}`} 
        //                                                     key={index}></button>)
        return (
                <div>
                    <div className="container">
                            <div className="card">
                                <div className="container-fliud">
                                    <div className="wrapper row">
                                        {/* <ImagesThumbnail images={img}/> */}
                                        <div className="details col-md-6">
                                            <h3 className="product-title">{}</h3>
                                            {/* <div className="rating">
                                                <div className="stars">
                                                    <span className="fa fa-star checked"></span>
                                                    <span className="fa fa-star checked"></span>
                                                    <span className="fa fa-star checked"></span>
                                                    <span className="fa fa-star"></span>
                                                    <span className="fa fa-star"></span>
                                                </div>
                                                <span className="review-no">41 reviews</span>
                                            </div> */}
                                            <ul className="product-description">
                                                {/* { desc } */}
                                            </ul>
                                            {/* <h4 className="price">current price: <span>₹{price}</span></h4> */}
                                            <h5 className="sizes">sizes:
                                                {/* { size } */}
                                            </h5>
                                            <h5 className="colors">colors:
                                                {/* { color } */}
                                            </h5>
                                            <div className="action">
                                                <button className="add-to-cart btn btn-default" type="button">add to cart</button>
                                                <button className="like btn btn-default" type="button"><span className="fa fa-heart"></span></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
        )
    }
}
