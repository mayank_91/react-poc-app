import React from 'react'

function ImagesThumbnail(props) {
    const imgStyle = {
       width: '30vw' ,
       height: '70vh'
    }
    const thumbnailImgStyle = {
        width: '8vw',
        height: '15vh'
    }
    console.log(props)
    const {images} = props
    const image = images.map((img, index)=><div className={`tab-pane ${img.type ? 'active' : ''}`} 
                                                    key={index} id={index}>
                                                    <img style={imgStyle} src={img.url} />
                                            </div>)
    const thumbnail = images.map((img, index)=> <li  className={`${img.type ? 'active' : ''}`}  key={index}><a data-target={`#${index}`} data-toggle="tab">
                                                        <img style={thumbnailImgStyle} src={img.url} />
                                                    </a>
                                                </li>)
    return (
        <div className="preview col-md-6">                      
            <div className="preview-pic tab-content">
                {image}
            </div>
            <ul className="preview-thumbnail nav nav-tabs">
                {thumbnail}
            </ul>
        </div>
    )
}

export default ImagesThumbnail
