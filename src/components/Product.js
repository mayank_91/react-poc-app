import React, { Component } from 'react'
import axios from 'axios'
import ProductView from './ProductView';

export default class Product extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            products: [],
            dataLoading: true
        }
    }
    
    componentDidMount(){
        
        axios.get("http://localhost:8080/config")
            .then(response=>{
                this.setState({
                    products: response.data,
                    dataLoading: false})
                    
                console.log(response)
            })
            .catch(error =>{
                this.setState({
                    dataLoading: false
                })
                console.log(error);
            })
    }

    render() {
        
        const { products } = this.state;
        console.log(products)
        const view = products ? 
                        products.map(product => <ProductView productConfiguration={products} 
                        key={product.name}
                        />) 
                        :
                        'DATA LOADING...';
        return ( 
            <div>
               { view }
            </div>
        )
    }
}
