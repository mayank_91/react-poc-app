import React from 'react';
import './App.css';
import './css/headermenu.css';
import './css/productsList.css';
import NavBar from './components/Navbar'
import ProductsList from './components/ProductsList'
import Product from './components/Product'

function App() {
  return (
    <div className="App">
     <NavBar />
     {/* <br/>
     <br/>
     <br/>
     <hr /> */}
     {/* <ProductsList /> */}
     <br /><br /><br /><br /><br />
     <Product />
    </div>
  );
}

export default App;
